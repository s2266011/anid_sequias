# ANID_Sequias

Repository of the code in R programming language that is needed to compute drought indicators and prepare the data for interactive visualization using Tableau software.
This repository is encompassed by the FAIR principle.